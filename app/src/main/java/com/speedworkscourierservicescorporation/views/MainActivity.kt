package com.speedworkscourierservicescorporation.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.speedworkscourierservicescorporation.BR
import com.speedworkscourierservicescorporation.R
import com.speedworkscourierservicescorporation.adapters.GenericAdapter
import com.speedworkscourierservicescorporation.base.BaseActivity
import com.speedworkscourierservicescorporation.extensions.hintSetup
import com.speedworkscourierservicescorporation.interfaces.ItemClickInterface
import com.speedworkscourierservicescorporation.models.ServicesModel
import com.speedworkscourierservicescorporation.utils.ScreenType
import com.speedworkscourierservicescorporation.views.fragments.AboutUsFragment
import com.speedworkscourierservicescorporation.views.fragments.ContactUsFragment
import com.speedworkscourierservicescorporation.views.fragments.HomeFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_drawer.*

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    var mDrawerToggle: ActionBarDrawerToggle? = null

    override fun loadLayout() {
        setContentView(R.layout.activity_main_drawer)
    }

    override fun init() {
//        mDrawerToggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.sentence_navigation_drawer_open, R.string.sentence_navigation_drawer_close)
        changeContent(ScreenType.HOME)
        navView.setNavigationItemSelectedListener(this)

        ivmenu?.setOnClickListener {
            drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    fun changeContent(screen: ScreenType) {
        var fragment: Fragment? = null
        when(screen) {
            ScreenType.HOME -> {
                fragment = HomeFragment()
            }
            ScreenType.ABOUTUS -> {
                fragment = AboutUsFragment()
            }
            ScreenType.CONTACTUS -> {
                fragment = ContactUsFragment()
            }
        }

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_layout, fragment)
            addToBackStack(screen.toString())
            commit()
        }
        drawerLayout.closeDrawer(GravityCompat.START)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.nav_home -> {
                changeContent(ScreenType.HOME)
                return true
            }
            R.id.nav_aboutus -> {
                changeContent(ScreenType.ABOUTUS)
                return true
            }
            R.id.nav_phone -> {
                changeContent(ScreenType.CONTACTUS)
                return true
            }
        }
        return false
    }
}