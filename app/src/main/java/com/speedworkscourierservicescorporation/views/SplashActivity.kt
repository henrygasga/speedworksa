package com.speedworkscourierservicescorporation.views

import android.view.Window
import android.view.WindowManager
import com.speedworkscourierservicescorporation.R
import com.speedworkscourierservicescorporation.base.BaseActivity
import com.speedworkscourierservicescorporation.extensions.launchActivity

class SplashActivity : BaseActivity() {

    override fun loadLayout() {
        setContentView(R.layout.activity_splash)
    }

    override fun init() {
        val w: Window = window
        w.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
    }

    fun moveToHome() {
        launchActivity<MainActivity> {  }
        finish()
    }
}