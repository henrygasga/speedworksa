package com.speedworkscourierservicescorporation.views

import android.graphics.drawable.ClipDrawable
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.speedworkscourierservicescorporation.BR
import com.speedworkscourierservicescorporation.R
import com.speedworkscourierservicescorporation.adapters.GenericAdapter
import com.speedworkscourierservicescorporation.base.BaseActivity
import com.speedworkscourierservicescorporation.extensions.launchActivity
import com.speedworkscourierservicescorporation.interfaces.ItemClickInterface
import com.speedworkscourierservicescorporation.models.TrackingModel
import kotlinx.android.synthetic.main.activity_searchresult.*

class SearchResultActivity : BaseActivity() {

    lateinit var trackList: ArrayList<TrackingModel>

    override fun loadLayout() {
        setContentView(R.layout.activity_searchresult)
        trackList = arrayListOf()
    }

    override fun init() {
        ivback.setOnClickListener {
            onBackPressed()
        }

        trackList.add(TrackingModel("",""))
        trackList.add(TrackingModel("",""))
        trackList.add(TrackingModel("",""))
        trackList.add(TrackingModel("",""))
        trackList.add(TrackingModel("",""))

        rvresults.apply {
            layoutManager = LinearLayoutManager(this@SearchResultActivity)
            adapter = GenericAdapter(trackList, R.layout.item_rv_result, BR.trackingmodel,
            object : ItemClickInterface<TrackingModel> {
                override fun onItemClick(v: View?, item: TrackingModel, position: Int) {
                    launchActivity<TrackingActivity> {  }
                }
            })
            addItemDecoration(DividerItemDecoration(this@SearchResultActivity, ClipDrawable.HORIZONTAL))
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}