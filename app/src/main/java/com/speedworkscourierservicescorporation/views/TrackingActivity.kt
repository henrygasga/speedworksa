package com.speedworkscourierservicescorporation.views

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.speedworkscourierservicescorporation.BR
import com.speedworkscourierservicescorporation.R
import com.speedworkscourierservicescorporation.adapters.GenericAdapter
import com.speedworkscourierservicescorporation.base.BaseActivity
import com.speedworkscourierservicescorporation.interfaces.ItemClickInterface
import com.speedworkscourierservicescorporation.models.TrackingDeliveryDetailsModel
import kotlinx.android.synthetic.main.activity_tracking.*

class TrackingActivity : BaseActivity() {

    lateinit var trackinglist: ArrayList<TrackingDeliveryDetailsModel>

    override fun loadLayout() {
        setContentView(R.layout.activity_tracking)
        trackinglist = arrayListOf()
    }

    override fun init() {
        ivback.setOnClickListener {
            onBackPressed()
        }

        trackinglist.add(TrackingDeliveryDetailsModel("","", false, true, "", ""))
        trackinglist.add(TrackingDeliveryDetailsModel("","", true, true, "", ""))
        trackinglist.add(TrackingDeliveryDetailsModel("","", true, true, "", ""))
        trackinglist.add(TrackingDeliveryDetailsModel("","", true, true, "", ""))
        trackinglist.add(TrackingDeliveryDetailsModel("","", true, true, "", ""))
        trackinglist.add(TrackingDeliveryDetailsModel("","", true, true, "", ""))
        trackinglist.add(TrackingDeliveryDetailsModel("","", true, true, "", ""))
        trackinglist.add(TrackingDeliveryDetailsModel("","", true, true, "", ""))
        trackinglist.add(TrackingDeliveryDetailsModel("","", true, true, "", ""))
        trackinglist.add(TrackingDeliveryDetailsModel("","", true, false, "", ""))

        rvtrackingdet.apply {
            layoutManager = LinearLayoutManager(this@TrackingActivity)
            adapter = GenericAdapter(trackinglist, R.layout.item_rvtracking, BR.trackdetails,
            object : ItemClickInterface<TrackingDeliveryDetailsModel>{
                override fun onItemClick(
                    v: View?,
                    item: TrackingDeliveryDetailsModel,
                    position: Int
                ) {

                }
            })
        }
    }
}