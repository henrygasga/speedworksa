package com.speedworkscourierservicescorporation.views.fragments

import com.speedworkscourierservicescorporation.R
import com.speedworkscourierservicescorporation.base.BaseFragment

class AboutUsFragment : BaseFragment() {

    override fun init() {

    }

    override fun setLayoutView(): Int {
        return R.layout.fragment_aboutus
    }

    override fun layoutLoaded() {

    }
}