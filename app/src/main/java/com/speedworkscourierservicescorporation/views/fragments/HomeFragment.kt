package com.speedworkscourierservicescorporation.views.fragments

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.speedworkscourierservicescorporation.BR
import com.speedworkscourierservicescorporation.R
import com.speedworkscourierservicescorporation.adapters.GenericAdapter
import com.speedworkscourierservicescorporation.base.BaseFragment
import com.speedworkscourierservicescorporation.extensions.hintSetup
import com.speedworkscourierservicescorporation.extensions.launchActivity
import com.speedworkscourierservicescorporation.interfaces.ItemClickInterface
import com.speedworkscourierservicescorporation.models.ServicesModel
import com.speedworkscourierservicescorporation.views.SearchResultActivity
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment() {

    override fun init() {

    }

    override fun setLayoutView(): Int {
        return R.layout.fragment_home
    }

    override fun layoutLoaded() {
        ettrack.hintSetup("Search Tracking")

        var myservices: ArrayList<ServicesModel>? = arrayListOf()
        myservices?.add(ServicesModel("Door to Door Delivery",""))
//        myservices?.add(ServicesModel("",""))
//        myservices?.add(ServicesModel("",""))
//        myservices?.add(ServicesModel("",""))

        llsearchbtn.setOnClickListener {
            mActivity.launchActivity<SearchResultActivity> {  }
        }

        rvservices.apply {
            layoutManager = LinearLayoutManager(mActivity)
            adapter = GenericAdapter(myservices?.toList()!!, R.layout.item_rv_services, BR.model,
                object : ItemClickInterface<ServicesModel> {
                    override fun onItemClick(v: View?, item: ServicesModel, position: Int) {

                    }
                })
        }
    }
}