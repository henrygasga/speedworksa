package com.speedworkscourierservicescorporation.views.fragments

import com.speedworkscourierservicescorporation.R
import com.speedworkscourierservicescorporation.base.BaseFragment

class ContactUsFragment : BaseFragment() {

    override fun init() {

    }

    override fun setLayoutView(): Int {
        return R.layout.fragment_contactus
    }

    override fun layoutLoaded() {

    }
}