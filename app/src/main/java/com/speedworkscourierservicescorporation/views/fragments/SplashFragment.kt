package com.speedworkscourierservicescorporation.views.fragments

import androidx.fragment.app.Fragment
import com.speedworkscourierservicescorporation.R
import com.speedworkscourierservicescorporation.base.BaseFragment
import com.speedworkscourierservicescorporation.views.SplashActivity
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class SplashFragment : BaseFragment(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    override fun init() {

    }

    override fun setLayoutView(): Int {
        return R.layout.fragment_splash
    }

    override fun layoutLoaded() {
        launch {
            delay(1000)
            withContext(Dispatchers.Main) {
                (mActivity as SplashActivity).moveToHome()
            }
        }
    }
}