package com.speedworkscourierservicescorporation.interfaces

import android.view.View

interface ItemClickInterface<T> {
    fun onItemClick(v:View?, item: T, position: Int)
}