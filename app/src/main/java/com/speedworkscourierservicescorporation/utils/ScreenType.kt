package com.speedworkscourierservicescorporation.utils

enum class ScreenType {
    HOME,
    ABOUTUS,
    CONTACTUS
}