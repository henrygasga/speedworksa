package com.speedworkscourierservicescorporation.extensions

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.google.android.material.textfield.TextInputEditText

inline fun <reified T : Activity> Activity.launchActivity(extras: Bundle? = null, noinline block: Intent.() -> Unit = {}) {
    val intent = Intent(this, T::class.java)
    if (extras != null) intent.putExtras(extras)
    intent.block()
    startActivity(intent)
}

fun TextInputEditText.hintSetup(text: String) {
    this.hint = text
    this.setOnFocusChangeListener { v, hasFocus ->
        if(hasFocus) {
            this.hint = ""
        } else {
            this.hint = text
        }
    }
}