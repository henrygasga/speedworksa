package com.speedworkscourierservicescorporation.models

data class TrackingDeliveryDetailsModel(
    var datem: String,
    var datesub: String,
    var showtop: Boolean,
    var showbottom: Boolean,
    var details: String,
    var detailssub: String
)