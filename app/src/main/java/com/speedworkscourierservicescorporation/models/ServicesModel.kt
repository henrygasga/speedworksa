package com.speedworkscourierservicescorporation.models

data class ServicesModel(
    var servicename: String,
    var servicedescription: String
)